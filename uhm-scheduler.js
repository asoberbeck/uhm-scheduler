var day_abbrevs = ["m", "t", "w", "r", "f", "s"];

var candidates = [];

function debug_print(s)
{
    document.getElementById("debug_list").innerHTML += "<li>" + s + "</li>";
}

function add_a_section()
{
    var title = document.getElementById("title_input").value;
    var crn = document.getElementById("crn_input").value;
    var days1 = document.getElementById("days_input1").value;
    var time1 = document.getElementById("time_input1").value;
    var days2 = document.getElementById("days_input2").value;
    var time2 = document.getElementById("time_input2").value;
    var days3 = document.getElementById("days_input3").value;
    var time3 = document.getElementById("time_input3").value;

    var section = {};

    if (title === "")
    {
	return;
    }
    section["title"] = title;

    if (crn === "")
    {
	return;
    }
    section["crn"] = crn;

    section["times"] = {};
    var days = days_of_text(days1);

    section_maybe_add_days_time(section, days1, time1);
    section_maybe_add_days_time(section, days2, time2);
    section_maybe_add_days_time(section, days3, time3);

    candidates.push(section);
    display_candidates();
}

function section_maybe_add_days_time(section, days_text, time_text)
{
    var days = days_of_text(days_text);
    if (days !== null)
    {
	var time = startend_of_text(time_text);
	if (time !== null)
	{
	    for (var i = 0; i < days.length; i++)
	    {
		var current_day = days[i];
		// if section doesn't have any day entries for this day,
		//     create it
		if (section.times[current_day] === undefined) 
		{
		    section.times[current_day] = [];
		}
		// add time range to this day
		section.times[current_day].push(time);
	    }
	    return true;	// successfully added info to section
	}
    }
    return;
}

function show_candidate(n)
{
    document.getElementById("course_output").innerHTML = calendar([candidates[n]]);
}

function delete_candidate(n)
{
    candidates.splice(n, 1);
    display_candidates();
}

function delete_all_candidates()
{
    candidates = [];
    display_candidates();
}

function display_candidates()
{
    var new_list = [];
    var e = document.getElementById("course_list");

    for (var i = 0; i < candidates.length; i++)
    {
	var section = candidates[i];
	new_list.push("<li>");
	new_list.push("title: " + section.title + "; crn: " + section.crn + "; ");
	for (var j = 0; j < day_abbrevs.length; j++)
	{
	    var day = day_abbrevs[j];
	    if (section.times[day])
	    {
		new_list.push(day + ": " + section.times[day].join("|") + "  ");
	    }
	}
	new_list.push("<input type=\"button\" value=\"show\" onclick=\"show_candidate(" + i + ");\">");
	new_list.push("<input type=\"button\" value=\"remove\" onclick=\"delete_candidate(" + i + ");\">");
	new_list.push("</li>");
    }

    e.innerHTML = new_list.join("");
}

function cmp_title(a, b)
{
    var title_a = a.title, title_b = b.title;
    if (title_a > title_b) return 1;
    if (title_a < title_b) return -1;
    return 0;
}

function sort_candidates()
{
    candidates.sort(cmp_title);
    display_candidates();
}

var schedule_index = 0;
var schedules = [];
var n_schedules = 0;

function schedule_candidates()
{
    // schedules = semester_me(candidates);
    // list flattening... asterisk ampersand pound sign exclamation
    // point exclamation point exclamation point
    schedules = [];
    semester_me(candidates);

    n_schedules = schedules.length;
    schedule_index = 0;

    redraw_schedule();
}

function view_schedule(offset)
{
    schedule_index += parseInt(offset, 10);
    // schedule_index = 0;
    // mod operator dislikes negative arguments...
    while (schedule_index >= n_schedules) schedule_index -= n_schedules;
    while (schedule_index < 0) schedule_index += n_schedules;

    redraw_schedule();
}

function redraw_schedule()
{
    document.getElementById("schedule_index").innerHTML = schedule_index + " of " + (n_schedules - 1);

    document.getElementById("course_output").innerHTML = 
	calendar(schedules[schedule_index]);
}
