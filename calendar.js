// fixme: copy-pasta from calendar.js
function zeropad_time(time)
{
    time = time + "";		// arrgh
    if (time.length == 3) return "0" + time;
    if (time.length == 2) return "00" + time;
    if (time.length == 1) return "000" + time;
    if (time.length == 0) return "0000" + time;
    return time;
}

// fixme: copy-pasta from calendar.js
function hhmm_of_time(time)
{
    return [parseInt(time/100), time % 100];
}

// fixme: copy-pasta from calendar.js
function calendar(sections)
{
    var days = ["m", "t", "w", "r", "f", "s"];

    var cells = {"m": {}, "t": {}, "w": {}, "r": {}, "f": {}, "s": {}};

    // (For each section)
    for (var sections_i = 0; sections_i < sections.length; sections_i++)
    {	
	var section = sections[sections_i];

	var times = section.times;
	for (var i = 0; i < days.length; i++)
	{
	    var day = days[i];
	    var meetings = times && times[day] && times[day].length;
	    if (meetings === undefined) continue;

	    // print("see entries in " + day);

	    var today_times = times[day];
	    for (var j = 0; j < today_times.length; j++)
	    {
		var start_time = today_times[j][0], end_time = today_times[j][1];
		var start = hhmm_of_time(start_time), end = hhmm_of_time(end_time);
		var start_hour = start[0], start_minute = start[1];
		var end_hour = end[0], end_minute = end[1];
		var cell_start_minute = parseInt(start_minute/20) * 20;
		var skipping = false;
		// print([start_hour, start_minute, end_hour, end_minute, cell_start_minute]);

		var hour = start_hour, minute = cell_start_minute;
		var time = 100 * hour + minute;
		var rowspan = 1;
		while (time < end_time)
		{
		    minute += 20;
		    if (minute >= 60)
		    {
			hour += 1;
			minute -= 60;
		    }
		    time = 100 * hour + minute;
		    if (skipping === false)
		    {
			// print(day + " " + time + " H");
			cells[day][time] = [section.title, 1, section.crn];
			var head_cell_time = time;
			skipping = true;
		    } else {
			// print(day + " " + time + " S");
			cells[day][time] = null;
			rowspan += 1;
		    }
		    // print(cells[day][time]);
		}
		if (skipping === true)
		{
		    cells[day][head_cell_time] = [section.title, rowspan, section.crn];
		}
	    }
	}
    }

    // Construct row primary HTML table
    // fixme: hour, minute, time, day defined earlier in this scope
    var table = ["<table>\n"];

    table.push("  <tr><td>Hour</td><td>M</td><td>T</td><td>W</td><td>R</td><td>F</td><td>S</td></tr>\n");

    for (var hour = 6; hour < 20; hour++)
    {
    	for (var minute = 0; minute < 60; minute += 20)
    	{
    	    var time = 100 * hour + minute;
    	    var row = ["  <tr>"];
	    if (minute == 0)
	    {
		row.push("<td>" + zeropad_time(time) + "</td>");
	    } else {
		row.push("<td>&nbsp;</td>");
	    }
    	    for (var day_index = 0; day_index < days.length; day_index++)
    	    {
    		var day = days[day_index];
		var this_cell = cells[day][time];
		if (this_cell === undefined)
		{
		    row.push("<td></td>");
		} else if (this_cell === null) {
		    // overlapped by a square -- skip this column
		} else {
		    var this_cell_title = this_cell[0];
		    var this_cell_rowspan = this_cell[1];
		    var this_cell_crn = this_cell[2];
		    row.push("<td rowspan=\"" + this_cell_rowspan + "\" style=\"border: solid 2px;\">" + this_cell_title + "<br>" + this_cell_crn + "</td>");
		}
    		// print(day + " " + time + " -> " + this_cell);
    	    }
	    row.push("</tr>\n");
	    table.push(row.join(""));
    	    // print(row);
    	}
    }

    table.push("</table>");

    return table.join("");
}
