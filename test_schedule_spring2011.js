// Test cases from choose_classes.py dated 2010-11-22 (Spring 2011 courses)

var spring_2011_courses = [];

// 81955	MATH 244	MWF	0830-0920a
spring_2011_courses.push({title: "MATH 244", crn: "81955", times: {m: [[830, 920]], w: [[830, 920]], f: [[830, 920]]}});

// 81956	MATH 244	TR	0900-1015a
spring_2011_courses.push({title: "MATH 244", crn: "81956", times: {t: [[900, 1015]], r: [[900, 1015]]}});

// 81957	MATH 244	TR	1200-0115p
spring_2011_courses.push({title: "MATH 244", crn: "81957", times: {t: [[1200, 1315]], r:[[1200, 1315]]}});

// 89036	MATH 244	TR	0130-0245p
spring_2011_courses.push({title: "MATH 244", crn: "89036", times: {t: [[1330, 1445]], r: [[1330, 1445]]}});

// 84173	MATH 307	MWF	0930-1020a
spring_2011_courses.push({title: "MATH 307", crn: "84173", times: {m: [[930, 1020]], w: [[930, 1020]], f: [[930, 1020]]}});

// 83554	EE 211		MWF	1130-1220p	T	0900-1150a
spring_2011_courses.push({title: "EE 211", crn: "83554", times: {m: [[1130, 1220]], t: [[900, 1150]], w: [[1130, 1220]], f: [[1130, 1220]]}});

// 83555	EE 211		MWF	1130-1220p 	T 	0130-0420p
spring_2011_courses.push({title: "EE 211", crn: "83555", times: {m: [[1130, 1220]], t: [[1330, 1620]], w: [[1130, 1220]], f: [[1130, 1220]]}});

// 82586	PHYS 272	MWF 	0830-0920a
spring_2011_courses.push({title: "PHYS 272", crn: "82586", times: {m: [[830, 920]], w: [[830, 920]], f: [[830, 920]]}});

// 82587	PHYS 272	TR 	1030-1145a
spring_2011_courses.push({title: "PHYS 272", crn: "82587", times: {t: [[1030, 1145]], r: [[1030, 1145]]}});

// 82588	PHYS 272L	M 	0130-0420p
spring_2011_courses.push({title: "PHYS 272L", crn: "82588", times: {m: [[1330, 1620]]}});

// 82589	PHYS 272L	T 	1200-0250p
spring_2011_courses.push({title: "PHYS 272L", crn: "82589", times: {t: [[1200, 1450]]}});

// 84249	PHYS 272L	T 	0300-0550p
spring_2011_courses.push({title: "PHYS 272L", crn: "84249", times: {t: [[1500, 1750]]}});

// 82590	PHYS 272L	W 	0130-0420p
spring_2011_courses.push({title: "PHYS 272L", crn: "82590", times: {w: [[1330, 1620]]}});

// 84865	PHYS 272L	R 	1200-0250p
spring_2011_courses.push({title: "PHYS 272L", crn: "84865", times: {r: [[1200, 1450]]}});

// 89381	PHYS 272L	R 	0300-0550p
spring_2011_courses.push({title: "PHYS 272L", crn: "89381", times: {r: [[1500, 1750]]}});
