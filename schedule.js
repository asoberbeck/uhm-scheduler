// Candidates input: already filtered by enabled/disabled in ui
function semester_me (candidates)
{
    // Get required titles
    var titles_obj = {};
    for (var i in candidates)
    {
	titles_obj[candidates[i].title] = true;
    }
    var required_titles = [];
    for (var j in titles_obj)
    {
	required_titles.push(j);
    }
    required_titles.sort();

    var schedules = pick_schedules([], required_titles, candidates);
    return schedules;
}

function pick_schedules (schedule, required_titles, candidates)
{
    if (required_titles.length === 0)
    {
	// if required_titles is empty, then we've found a valid schedule
	schedules.push(schedule);
	return schedule;
    }

    if (candidates.length === 0) 
    {
	// if required_titles still has requirements, this branch
	// hasn't yielded a schedule
	return [];
    }

    var current_candidate = candidates[0];
    var current_section_title = current_candidate.title;

    var schedules_including_section, schedules_excluding_section;
    // Can we include this current_candidate in schedule?
    // does it intersect any courses in our schedule?
    if (intersect_any(current_candidate, schedule))
    {
	schedules_including_section = [];
    } else {
	schedules_including_section =
	    pick_schedules(schedule.concat(current_candidate),
			   exclude_required_title(required_titles, current_section_title),
			   exclude_section_title(tail(candidates), current_section_title));
    }

    schedules_excluding_section =
	pick_schedules(schedule,
		       required_titles,
		       tail(candidates));

    return schedules_including_section.concat(schedules_excluding_section);
}

function intersect_any(section, sections)
{
    for (var i in sections)
    {
	if (intersect(section, sections[i]))
	{
	    return true;
	}
    }
    return false;
}

function tail (l)
{
    return l.slice(1, l.length);
}

function exclude_section_title (sections, title)
{
    // return sections.filter(function(x){return x.title !== title;});
    var result = [];
    for (var i in sections)
    {
	if (sections[i].title !== title)
	{
	    result.push(sections[i]);
	}
    }
    return result;
}

function exclude_required_title(titles, title)
{
    // return titles.filter(function(x){return x !== title;});
    var result = [];
    for (var i in titles)
    {
	if (titles[i] !== title)
	{
	    result.push(titles[i]);
	}
    }
    return result;
}
