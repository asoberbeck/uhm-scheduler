load(['test.js']);
load(['test_schedule_spring2011.js']);
load(['intersect.js']);
load(['schedule.js']);

print(spring_2011_courses.length); // -> 15
print(exclude_section_title(spring_2011_courses, "no-such-title").length); // -> 15

var titles = ["MATH 307", "MATH 244", "EE 211", "PHYS 272", "PHYS 272L"];

for (var i in titles)
{
    print(exclude_section_title(spring_2011_courses, titles[i]).length);
    // -> 14, 11, 13, 13, 9
}

var one_left = exclude_section_title(spring_2011_courses, null); // copy list
for (var i in titles)
{
    if (titles[i] === "MATH 307") continue;
    one_left = exclude_section_title(one_left, titles[i]);
}
print(one_left.length);		// -> 1


print(semester_me(spring_2011_courses).length);
