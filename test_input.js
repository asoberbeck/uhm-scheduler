load(['test.js']);
load(['input.js']);

print(days_of_text("mtwf"));	// -> m,t,w,f
print(days_of_text("MWfS"));	// -> m,w,f,s
print(days_of_text("MWqfS"));	// -> null and whine
print(days_of_text(""));	// -> null and don't whine

print(startend_of_text("1030-1120a")); // -> [1030, 1120]
print(startend_of_text("0300-0415p")); // -> [1500, 1615]
print(startend_of_text("0830-0920a")); // -> [830, 920]
print(startend_of_text("1200-0115p")); // -> [1200, 1315]
print(startend_of_text("1130-1220p")); // -> [1130, 1220]

print(startend_of_text("123asdfasdf")); // -> null and whine
