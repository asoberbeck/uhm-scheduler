function intersect (s1, s2)
{
    var t1 = s1.times;
    var t2 = s2.times;

    var day;
    var days = ["m", "t", "w", "r", "f", "s"];

    for (var i = 0; i < days.length; i++)
    {
	day = days[i];

	// Number of times each section meets on this specific day.
	var meetings1 = t1 && t1[day] && t1[day].length;
	var meetings2 = t2 && t2[day] && t2[day].length;

	// If either section does not meet on a specific day, they
	// can't intersect.
	if (meetings1 === undefined || meetings2 === undefined)
	{
	    continue;
	}

	// Compare each meeting time of course 1...
	for (var j1 = 0; j1 < meetings1; j1++)
	{
	    var start1 = t1[day][j1][0], end1 = t1[day][j1][1];
	    // ... to each meeting time of course 2:
	    for (var j2 = 0; j2 < meetings2; j2++)
	    {
		var start2 = t2[day][j2][0], end2 = t2[day][j2][1];
		if (end1 > start2 && end2 > start1)
		{
		    return true;
		}
	    }
	}
    }

    return false;
}
