// fixme: copy-pasta
function days_of_text(s)
{
    var unrec_abbrev = s.match(/[^mtwrfsMTWRFS]/);
    if (unrec_abbrev)
    {
	return null;
    }
    if (s === "")
	return null;

    // fixme: doesn't handle duplicated day abbreviations
    return s.toLowerCase().split("");
}

// fixme: more copy-pasta
function startend_of_text(s)
{
    if (s.match(/^[0-9]{4}/) == null)
    {
	return null;
    }
    if (s.slice(4, 5) !== "-")
    {
	return null;
    }
    if (s.match(/^[0-9]{4}-[0-9]{4}/) == null)
    {
	return null;
    }
    var ampm = s.slice(-1);
    if (ampm.match(/[ap]/) == null)
    {
	return null;
    }
    var start = parseInt(s.slice(0, 4), 10);
    var end = parseInt(s.slice(5, 9), 10);

    // schedule time format is ambiguous...
    if (ampm === "p")
    {
	if (end < 1200)
	    end += 1200;
	if (start < 900)
	    start += 1200;
    }

    // fixme: doesn't handle nonsensical time values, e.g. 2599
    return [start, end];
}
