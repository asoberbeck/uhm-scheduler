load(['test.js']);
load(['intersect.js']);

// A section intersects with itself:
print(intersect(section1, section1));

// Sections 1 and 2 intersect on Friday:
print(intersect(section1, section2));

// Sections 1 and 3 do not intersect:
print(intersect(section1, section3));

// Sections 2 and 3 intersect on Monday:
print(intersect(section2, section3));
