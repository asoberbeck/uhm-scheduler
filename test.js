// Common test functions
function debug_print(s)
{
    print(s);
}

// Common test cases
var section1 = {title: "MATH 307", 
		crn: "73760", 
		times: {m: [[930, 1020]],
			w: [[930, 1020]],
			f: [[930, 1020]]}
};

var section2 = {title: "MATH 242",
		crn: "72014",
		times: {m: [[830, 920]],
			w: [[830, 920]],
			f: [[830, 920], [930, 1020]]}
};

var section3 = {title: "MATH 242",
		crn: "77539",
		times: {m: [[830, 920]],
			t: [[1200, 1315]],
			r: [[1200, 1315]]}
};
